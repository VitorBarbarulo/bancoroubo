package br.com.bacoroubo;

public class Conta {

	public static double saldoBanco;

	// atributos
	private int numero;
	private TipoConta tipoConta;
	private double saldo;
	private Cliente Cliente;

	// construtor
	public Conta(int numero, TipoConta tipoConta, double saldo, Cliente cliente) {
		super();
		this.numero = numero;
		this.tipoConta = tipoConta;
		this.saldo = saldo;
		Cliente = cliente;
	}

	public Cliente getCliente() {
		return Cliente;
	}

	public void setCliente(Cliente cliente) {
		Cliente = cliente;
	}

	// getters e setters
	public int getNumero() {
		return numero;
	}

	public void setNumero(int numero) {
		this.numero = numero;
	}

	public TipoConta getTipoConta() {
		return tipoConta;
	}

	public void setTipoConta(TipoConta tipoConta) {
		this.tipoConta = tipoConta;
	}

	public double getSaldo() {
		return saldo;
	}

	public void setSaldo(double saldo) {
		this.saldo = saldo;
	}

	// metodo
	public void exibeSaldo() {
		System.out.println("Salso: " + this.saldo);
	}

	public void saca(double valor) {
		Conta.saldoBanco -= valor;
		saldo -= valor;
	}

	public void deposita(double valor) {
		saldo += valor;
		Conta.saldoBanco += valor;
	}

	public void transferePara(Conta destino, double valor) {
		this.saca(valor);
		destino.deposita(valor);
	}

}
