package br.com.bacoroubo;

public enum TipoConta {
	
	CORRENTE("Conta Corrente"),
	SALARIO("Conta Sal�rio"),
	POUPANCA("Conta Poupan�a");
	
	public String titulo;
	TipoConta(String titulo) {
		this.titulo = titulo;
}
}