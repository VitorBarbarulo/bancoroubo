package br.com.bacoroubo;

public class bancoroubo {
	public static void main(String[] args) {

		Conta conta = new Conta(1, TipoConta.CORRENTE, 1000, new Cliente("Vitor", "45664935864", "15121126664", Sexo.MASCULINO));
		// conta.setNumero(1);
		// conta.setTipo("Conta Corrente");
		// conta.setSaldo(1000);

		Conta.saldoBanco = 1_000_000.00;

		conta.saca(100);

		conta.exibeSaldo();
		// System.out.println(conta.getSalso());

		conta.deposita(50);

		conta.exibeSaldo();

		// dados da conta
		System.out.println("DADOS DA CONTA: ");
		System.out.println("N�: " + conta.getNumero());
		System.out.println("Tipo: " + conta.getTipoConta().titulo);
		System.out.println("Saldo: " + conta.getSaldo());
		System.out.println("Titular: " + conta.getCliente().getNome());
		System.out.println("CPF: " + conta.getCliente().getCpf());
		System.out.println("RG: " + conta.getCliente().getRg());

	}
}
