package br.com.bacoroubo;

public class Cliente {

	// atributos
	private String nome;
	private String cpf;
	private String rg;
	private Sexo sexo;

	// construtor
	
	public Cliente(String nome, String cpf, String rg, Sexo sexo) {
		this.nome = nome;
		this.cpf = cpf;
		this.rg = rg;
		this.sexo = sexo;
	}
	

	// gtes e sets
	
	public String getNome() {
		return nome;
	}
	
	public void setNome(String nome) {
		this.nome = nome;
	}

	public Sexo getSexo() {
		return sexo;
	}


	public void setSexo(Sexo sexo) {
		this.sexo = sexo;
	}

	public String getCpf() {
		return cpf;
	}

	public void setCpf(String cpf) {
		this.cpf = cpf;
	}

	public String getRg() {
		return rg;
	}

	public void setRg(String rg) {
		this.rg = rg;
	}

}
